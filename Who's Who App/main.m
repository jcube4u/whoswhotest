//
//  main.m
//  Who's Who App
//
//  Created by George, Jidh on 17/07/2013.
//  Copyright (c) 2013 George, Jidh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
