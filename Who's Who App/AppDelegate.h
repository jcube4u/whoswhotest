//
//  AppDelegate.h
//  Who's Who App
//
//  Created by George, Jidh on 17/07/2013.
//  Copyright (c) 2013 George, Jidh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
