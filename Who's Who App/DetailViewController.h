//
//  DetailViewController.h
//  Who's Who App
//
//  Created by George, Jidh on 17/07/2013.
//  Copyright (c) 2013 George, Jidh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;


@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobTitle;
@property (weak, nonatomic) IBOutlet UITextView *biography;
@property (nonatomic, assign) BOOL showCircularImage;
@end
