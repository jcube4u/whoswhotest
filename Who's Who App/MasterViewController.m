//
//  MasterViewController.m
//  Who's Who App
//
//  Created by George, Jidh on 17/07/2013.
//  Copyright (c) 2013 George, Jidh. All rights reserved.
//

#import "MasterViewController.h"
#import "TFHpple.h"
#import "EmployeeProfile.h"
#import "DetailViewController.h"
#define WEBSITE_URL @"http://www.theappbusiness.com/people/"

@interface MasterViewController () {
}
@end

@implementation MasterViewController
@synthesize _objects;

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshData:)];
    self.navigationItem.rightBarButtonItem = refreshButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];

    [self refreshData:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshData:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }

    [self loadEmployeeData];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    

    EmployeeProfile *employee = _objects[indexPath.row];
    cell.textLabel.text = [employee name];
    cell.detailTextLabel.text = [employee jobTitle];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        NSDate *object = _objects[indexPath.row];
        self.detailViewController.detailItem = object;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        id object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
        
        if(indexPath.row %2 ==0 )
        {
            DetailViewController * controller = [segue destinationViewController];
            controller.showCircularImage = YES;
        }
            
    }
}


#pragma Request Data

-(void)loadEmployeeData
{
    // 1
    NSURL *websiteURL = [NSURL URLWithString:WEBSITE_URL];
    
    __weak MasterViewController *unretainedSelf = self;
    
    [unretainedSelf dataWithContentsOfURL:websiteURL completionBlock:^(NSData *data, NSError *error) {
        
        if(!error)
        {
            [unretainedSelf processData:data];
        }
        else
        {
        
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:error.domain message:[error.userInfo objectForKey:NSLocalizedDescriptionKey]  delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
            
            [alertView show];
        }
        
    }];


    
}

-(void) processData:(NSData*) data
{
    NSData *employeeHtmlData = data;
    
    // 2
    TFHpple *employeesParser = [TFHpple hppleWithHTMLData:employeeHtmlData];
    
    // 3
    NSString *empXpathQueryString = @"//div[@class='threecol profile ']";
    
    NSArray *empNodes = [employeesParser searchWithXPathQuery:empXpathQueryString];
    
    // 4
    NSMutableArray *newEmployees = [[NSMutableArray alloc] initWithCapacity:0];
    for (TFHppleElement *element in empNodes) {
        // 5
        EmployeeProfile *employee = [[EmployeeProfile alloc] init];
        [newEmployees addObject:employee];
        
        // 6
        for (TFHppleElement *child in element.children) {
            
            if ([child.tagName isEqualToString:@"img"]) {
                // 7
                @try {
                    employee.imageUrl = [child objectForKey:@"src"];
                }
                @catch (NSException *e) {}
            } else if ([child.tagName isEqualToString:@"h3"]) {
                // 8
                if(!employee.name)
                {
                    employee.name = [[child firstChild] content];
                }
                else
                {
                    employee.jobTitle = [[child firstChild] content];
                }
                
                
            } else if ([child.tagName isEqualToString:@"p"]) {
                // 9
                employee.biography = [[child firstChild] content];
            }
            
        }
    }
    
    // update the UI on main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        _objects = newEmployees;
        [self.tableView reloadData];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            if(_objects.count)
            {
                NSDate *object = [_objects objectAtIndex:0];
                self.detailViewController.detailItem = object;
            }
        }
    });

    
}

// Should go in to a category
- (void) dataWithContentsOfURL:(NSURL *)url completionBlock:(void (^)(NSData *data, NSError *error)) block {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        if(data) {
        
            block(data, nil);

        } else {
            NSError *error = [NSError errorWithDomain:@"ContentDownloadError"
                                                 code:1
                                             userInfo:[NSDictionary dictionaryWithObject:@"Content at URL is unavailable"
                                                                                  forKey:NSLocalizedDescriptionKey]];
            block(nil, error);
        }
        
    });
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait; // etc
}

@end
