//
//  Who_s_Who_AppTests.m
//  Who's Who AppTests
//
//  Created by George, Jidh on 17/07/2013.
//  Copyright (c) 2013 George, Jidh. All rights reserved.
//

#import "Who_s_Who_AppTests.h"
#import "MasterViewController.h"
#import "EmployeeProfile.h"

@implementation Who_s_Who_AppTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testConstruction_MasterDetail_shouldNotBeNil
{
    MasterViewController * controller  = [[MasterViewController alloc] init];
    
    STAssertTrue(nil != controller,@"Construction of the controller should not be nil");
}


- (void)testConstruction_EmployeeProfile_shouldNotBeNil
{
    EmployeeProfile * controller  = [[EmployeeProfile alloc] init];
    
    STAssertTrue(nil != controller,@"Construction of the EmployeeProfile should not be nil");
}



- (void)testProcessData_FromHTML_shouldPopulateEmployeeProfile
{
    MasterViewController *controller  = [[MasterViewController alloc] init];
    controller._objects = [[NSMutableArray alloc] init];
    
    NSString *thePath = [[NSBundle mainBundle] pathForResource:@"testData" ofType:@"html"];
    
    NSData *htmlData = [[NSData alloc]  initWithContentsOfFile:thePath];
    
    [controller processData:htmlData];
    
    STAssertTrue(([controller._objects count] > 0),@"Construction of the EmployeeProfile should not be nil");
}

@end
